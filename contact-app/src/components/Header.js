import React from 'react';

const Header = () => {
    return (
        <div className="ui fixed menu">
            <div className="ui container center" style={{padding:"1rem 0rem"}}>
                <h2>Contact Manager</h2>
            </div>
        </div>
    );
}

export default Header; 