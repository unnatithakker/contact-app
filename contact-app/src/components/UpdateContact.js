import React from 'react';

class UpdateContact extends React.Component{
    constructor(props){
        super(props);
        const {id, name, email} = props.location.state.contact;
        this.state = {
            id,
            name,
            email
        }
    }

    onChangeField = (e) => {
        let name = e.target.name;
        let value = e.target.value;
        this.setState({
            [name]: value
        });
    }

    update = (e) => {
        e.preventDefault();
        console.log(this.state);
        if(!this.state.name || !this.state.email){
            alert("All the fields are mandatory");
            return;
        }
        this.props.updateContactHandler(this.state);
        console.log(this.state);
        this.setState({name: "", email: ""}); 
        this.props.history.push("/")
    }

    render(){
        return (
            <div className="ui main">
            <h2>Add Contact</h2>
            <form className="ui form" onSubmit={this.update}>
                <div className="field">
                    <lable>Name</lable>
                    <input type="text" name="name" 
                    value={this.state.name}
                    placeholder="Enter Name" onChange={this.onChangeField} />
                </div>
                <div className="field">
                    <lable>email</lable>
                    <input type="text" name="email" 
                    value={this.state.email}
                    placeholder="Enter Name"
                    onChange={this.onChangeField} />
                </div>
                <button className="ui button blue">Add</button>
            </form>
            </div>
        )
    }
}

export default UpdateContact;