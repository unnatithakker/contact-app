import React, {useRef} from 'react';
// curly braces {} because Link is not default exported component 
import { Link } from 'react-router-dom';
import ContactCard from './ContactCard'

const ContactList = (props) => {
    const inputEl = useRef("");
    const deleteContactHandler = (id) => {
        props.getContactId(id);
    }

    const renderContactList = props.contacts.map((contact) => {
        return (
            <ContactCard contact={contact} clickHandler={deleteContactHandler} />
        )
    })

    const getSearchTerm = () => {
        props.searchKeyword(inputEl.current.value);
    }

    return (
        <div className="main">
            <div className="ui grid">
                <div className="thirteen wide column">
                <h2 className="ui header" style={{marginTop:"5rem"}}>Contact List</h2>
                </div>
                <div className="three wide column" style={{marginTop:"5rem"}}>
                <Link to="/add"><button className="ui button blue">Add Contact</button></Link>
                </div>
            </div>
           
            
            <div className="ui search">
                <div className="ui icon input">
                    <input ref={inputEl} type="text" placeholder="Search Contact" className="prompt" value={props.term} onChange={getSearchTerm}></input>
                    <i className="search icon"></i>
                </div>
            </div>
            <div className="ui celled list">
                {renderContactList.length > 0 ? renderContactList : "No Contacts Found"}
            </div>
        </div>
        
    )
}

export default ContactList;