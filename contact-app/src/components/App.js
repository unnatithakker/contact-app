import React, { useState, useEffect } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import './App.css';
import Header from "./Header";
import AddContact from './Addcontact';
import UpdateContact from './UpdateContact';
import ContactList from './ContactList';
import CardDetailContact from "./ContactDetail";
import { uuid } from 'uuidv4';
import api from '../api/contacts';

function App() {
  const LOCAL_STORAGE_KEY = "contacts";
  const [contacts, setContacts] = useState([]);
  const [searchTerm, setSearchTerm] = useState("");
  const [searchResult, setSearchResult] = useState([]);

  //Retrive Contacts
  const getContacts = async () => {
    const response = await api.get("/contacts");
    return response.data;
  }

  const addContactHandler = async (contact) => {
    const request = {
      id: uuid(),
      ...contact
    }

    const response = await api.post("/contacts", request);
    setContacts([...contacts, response.data]);
  }

  const removeContactHandler = async (id) => {
    await api.delete(`contacts/${id}`)
    const newContactList = contacts.filter((contact) => contact.id !== id);
    setContacts(newContactList);
  }

  const updateContactHandler = async (contact) => {
    const response = await api.put(`/contacts/${contact.id}`, contact);
    const { id, name, email } = response.data;
    setContacts(contacts.map(contact => {
      return contact.id === id ? { ...response.data } : contact;
    }))
  }

  const searchHandler = async(searchText) => {
    setSearchTerm(searchText);
    if(searchText){
      const newContactList = contacts.filter((contact) => {
        return Object.values(contact).join(" ").toLowerCase().includes(searchText.toLowerCase());
      });
      setSearchResult(newContactList);
    }else{
      setSearchResult(contacts);
    }
  }

  useEffect(() => {
    const receiveContacts = async () => {
      const allContacts = await getContacts();
      if (allContacts) {
        setContacts(allContacts);
      }
    };
    receiveContacts();
  }, []);

  useEffect(() => {
    localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(contacts));
  }, [contacts])

  return (
    <div className="ui container">
      <Header />
      <Router>
        <Switch>
          {/* When you give component-name inside component, react internally uses REACT.createElement. When you pass it inside 
          an anonymous function than eah time when we visit the route or URL everytime we will be creating element. Hence, 
          performance impact.  
          <Route path="/" exact component={() => <ContactList contacts={contacts} getContactId= {removeContactHandler} />}></Route>
          <Route path="/add" component={() => <AddContact addContactHandler={addContactHandler} />}></Route>
          */}

          <Route path="/" exact render={(props) => 
            (<ContactList {...props} 
            contacts={searchTerm.length < 1 ? contacts :  searchResult} 
            getContactId={removeContactHandler} 
            term={searchTerm} 
            searchKeyword={searchHandler} />)}></Route>
          <Route path="/add" render={(props) => (<AddContact {...props} addContactHandler={addContactHandler} />)}></Route>
          <Route path="/update" render={(props) => (<UpdateContact {...props} updateContactHandler={updateContactHandler} />)}></Route>
          <Route path="/contact/:id" render={(props) => (<CardDetailContact {...props} />)}></Route>
        </Switch>

        {/* <AddContact addContactHandler={addContactHandler} />
        <ContactList contacts={contacts} getContactId= {removeContactHandler} /> */}
      </Router>


    </div>
  );
}

export default App;
